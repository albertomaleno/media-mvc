package com.myhouse.mvc.dal.impl;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import com.myhouse.mvc.ITMongoDBContainerConfiguration;
import com.myhouse.mvc.dal.interfaces.MediaDAO;
import com.myhouse.mvc.model.Media;
import com.myhouse.mvc.model.MediaExtension;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;


@SpringBootTest
@RunWith(SpringRunner.class)
public class ITMongoDAOTest extends ITMongoDBContainerConfiguration {

    @Autowired
    private MongoMediaDAO mediaDAO;

    @BeforeClass
    public static void setUp(){
        mongoDBContainer.start();
    }

    @Test
    public void when_MediaDoesNotExist_Expect_NullMedia(){
        final String mediaName = "fake";

        assertNull(mediaDAO.findByName(mediaName));
    }

    @Test
    public void when_MediaIsPersisted_Expect_MediaRetrievable(){
        Media media = new Media();
        media.setId("1");
        media.setName("media");
        media.setDescription("description");
        media.setExtension(MediaExtension.Extension.JPG);

        mediaDAO.persist(media);
        Media extractedMedia = mediaDAO.findByName(media.getName());

        assertThat(media.getId(), is(extractedMedia.getId()));
        assertThat(media.getName(), is(extractedMedia.getName()));
        assertThat(media.getDescription(), is(extractedMedia.getDescription()));
        assertThat(media.getData(), is(extractedMedia.getData()));
        assertThat(media.getExtension(), is(extractedMedia.getExtension()));
        assertThat(media.getState(), is(extractedMedia.getState()));
        assertThat(extractedMedia.hashCode(), is(media.hashCode()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_MediaIsNull_Expect_IllegalArgumentException(){
        mediaDAO.persist(null);
    }

    @Test
    public void when_MediaIsRemoved_Expect_MediaNullOnRetrieve(){
        Media media = new Media();
        media.setId("1");
        media.setName("media");
        media.setDescription("description");
        media.setExtension(MediaExtension.Extension.JPG);

        mediaDAO.persist(media);
        mediaDAO.remove(media);

        assertNull(mediaDAO.findByName(media.getName()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_MediaToRemoveIsNull_Expect_IllegalArgumentException(){
        mediaDAO.remove(null);
    }


    @AfterClass
    public static void tearDown(){
        mongoDBContainer.stop();
    }
}
