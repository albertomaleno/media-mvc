package com.myhouse.mvc;

import org.junit.ClassRule;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.GenericContainer;


@ContextConfiguration(initializers = {ITMongoDBContainerConfiguration.Initializer.class})
public class ITMongoDBContainerConfiguration {

    private static final String MONGO_IMAGE = "mongo:latest";
    private static final Integer CONTAINER_PORT = 27017;

    @ClassRule
    public static GenericContainer mongoDBContainer =
            new GenericContainer(MONGO_IMAGE)
            .withExposedPorts(CONTAINER_PORT);


    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.data.mongodb.host=" + mongoDBContainer.getContainerIpAddress(),
                    "spring.data.mongodb.port=" + mongoDBContainer.getFirstMappedPort()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }
}
