package com.myhouse.mvc.exception;

import static org.junit.Assert.*;

import org.junit.Test;

public class ServiceErrorsTest {

    @Test
    public void when_GetIdIsCalled_Expect_ErrorId(){
        assertEquals(1, ServiceErrors.MEDIA_DATA_DIFFERS_FROM_MODEL.getId());
    }

    @Test
    public void when_GetIdMessageCalled_Expect_ErrorMessage(){
        assertEquals("No media could be found", ServiceErrors.MEDIA_DOES_NOT_EXIST.getMessage());
    }
}
