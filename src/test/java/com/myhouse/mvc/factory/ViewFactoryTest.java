package com.myhouse.mvc.factory;

import static org.junit.Assert.*;

import com.myhouse.mvc.model.Media;
import com.myhouse.mvc.model.MediaExtension;
import com.myhouse.mvc.model.MediaStatus;
import org.junit.Test;

public class ViewFactoryTest {

    private static final String PHOTO_VIEW = "show-photo";
    private static final String VIDEO_VIEW = "show-video";
    private static final String AUDIO_VIEW = "show-audio";

    @Test
    public void when_MediaExtensionIsFromAudio_Expect_AudioView(){
        Media media = new Media.MediaBuilder("")
                .withExtension(MediaExtension.Extension.FLAAC)
                .withDescription("blabliblu")
                .build();

        assertEquals(AUDIO_VIEW, ViewFactory.getView(media));
    }

    @Test
    public void when_MediaExtensionIsFromVideo_Expect_VideoView(){
        Media media = new Media.MediaBuilder("")
                .withExtension(MediaExtension.Extension.MKV)
                .withMediaState(MediaStatus.State.PROCESSING)
                .build();

        assertEquals(VIDEO_VIEW, ViewFactory.getView(media));
    }

    @Test
    public void when_MediaExtensionIsFromPhoto_Expect_PhotoView(){
        Media media = new Media.MediaBuilder("")
                .withExtension(MediaExtension.Extension.GIF)
                .build();

        assertEquals(PHOTO_VIEW, ViewFactory.getView(media));
    }
}
