/*
 * Copyright (C) 2020 alberto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.myhouse.mvc.controller;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.myhouse.mvc.ITMongoDBContainerConfiguration;
import com.myhouse.mvc.model.Media;
import com.myhouse.mvc.model.MediaExtension;
import com.myhouse.mvc.model.MediaStatus;
import com.myhouse.mvc.service.MediaService;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.servlet.FlashMap;

import java.util.Base64;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class ITMediaController extends ITMongoDBContainerConfiguration {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private MediaService mediaService;


    @BeforeClass
    public static void setUp() {
        mongoDBContainer.start();
    }

    @Test
    public void when_MediaIsSubmitted_Expect_ShowMediaInformation() throws Exception {
        Media media = new Media.MediaBuilder("photo1")
                .withDescription("noDesc")
                .withExtension(MediaExtension.Extension.JPG)
                .build();
        MockMultipartFile mockedFile = new MockMultipartFile("file", (media.getName() + "." + media.getExtension().getDisplayName()),
                MediaType.MULTIPART_FORM_DATA_VALUE, media.getName().getBytes());

        FlashMap sessionAttributes = mockMvc.perform(post("/media")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("name=" + media.getName() + "&description=" + media.getDescription() + "&extension=" + media.getExtension().toString()))
                .andExpect(redirectedUrl("/media/upload"))
        .andReturn().getFlashMap();

        mockMvc.perform(multipart("/media/upload")
                .file(mockedFile)
                .sessionAttrs(sessionAttributes)
                .param("name", media.getName()))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/media/show"));
    }

    @Test
    public void when_MediaSubmittedHasDataWithDifferentName_Expect_BadRequest() throws Exception {
        Media media = new Media.MediaBuilder("photo2")
                .withDescription("noDesc")
                .withExtension(MediaExtension.Extension.JPG)
                .build();
        MockMultipartFile mockedFile = new MockMultipartFile("file", ("FAKE" + ".jpg"),
                MediaType.MULTIPART_FORM_DATA_VALUE, media.getName().getBytes());

        FlashMap sessionAttributes = mockMvc.perform(post("/media")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("name=" + media.getName() + "&description=" + media.getDescription() + "&extension=" + media.getExtension().toString()))
                .andExpect(redirectedUrl("/media/upload"))
                .andReturn().getFlashMap();

        MvcResult result = mockMvc.perform(multipart("/media/upload")
                .file(mockedFile)
                .sessionAttrs(sessionAttributes)
                .param("name", media.getName()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();
        String exceptionMessage = result.getResolvedException().getMessage();
        assertThat(exceptionMessage, is("400 BAD_REQUEST \"Media data differs from information already present on database\""));
    }

    @Test
    public void when_MediaSubmittedHasDataWithDifferentExtension_Expect_BadRequest() throws Exception {
        Media media = new Media.MediaBuilder("photo3")
                .withDescription("noDesc")
                .withExtension(MediaExtension.Extension.JPG)
                .build();
        MockMultipartFile mockedFile = new MockMultipartFile("file", (media.getName() + ".png"),
                MediaType.MULTIPART_FORM_DATA_VALUE, media.getName().getBytes());

        FlashMap sessionAttributes = mockMvc.perform(post("/media")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("name=" + media.getName() + "&description=" + media.getDescription() + "&extension=" + media.getExtension().toString()))
                .andExpect(redirectedUrl("/media/upload"))
                .andReturn().getFlashMap();

        MvcResult result = mockMvc.perform(multipart("/media/upload")
                .file(mockedFile)
                .sessionAttrs(sessionAttributes)
                .param("name", media.getName()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();
        String exceptionMessage = result.getResolvedException().getMessage();
        assertThat(exceptionMessage, is("400 BAD_REQUEST \"Media data differs from information already present on database\""));
    }

    @Test
    public void when_PhotoIsAddedWithData_Expect_PhotoCorrectlyPersistedAndExtracted() throws Exception {
        Media media = new Media.MediaBuilder("photo4")
                .withDescription("noDesc")
                .withExtension(MediaExtension.Extension.JPG)
                .build();
        MockMultipartFile mockedFile = new MockMultipartFile("file", (media.getName() + "." + media.getExtension().getDisplayName()),
                MediaType.MULTIPART_FORM_DATA_VALUE, media.getName().getBytes());

        FlashMap sessionAttributes = mockMvc.perform(post("/media")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("name=" + media.getName() + "&description=" + media.getDescription() + "&extension=" + media.getExtension().toString()))
        .andReturn().getFlashMap();
        sessionAttributes = mockMvc.perform(multipart("/media/upload")
                .file(mockedFile)
                .sessionAttrs(sessionAttributes)
                .param("name", media.getName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/media/show"))
                .andReturn().getFlashMap();

        media = mediaService.retrieveMedia(media.getName());
        String encodedContent = mediaService.retrieveEncodedData(media);
        mockMvc.perform(get("/media/show")
                .sessionAttrs(sessionAttributes))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("show-photo"))
                .andExpect(model().attribute("media", allOf(
                        hasProperty("name", is(media.getName())),
                        hasProperty("description", is(media.getDescription())),
                        hasProperty("extension", is(media.getExtension())),
                        hasProperty("state", is(MediaStatus.State.PROCESSED)),
                        hasProperty("data", is(notNullValue())))))
                .andExpect(content().string(containsString(encodedContent)));
        assertThat(Base64.getDecoder().decode(encodedContent), is(mockedFile.getBytes()));
    }

    @Test
    public void when_PhotoIsAddedWithoutData_Expect_PhotoInformationAccessibleWithoutData() throws Exception {
        Media media = new Media.MediaBuilder("photo5")
                .withDescription("noDesc")
                .withExtension(MediaExtension.Extension.JPG)
                .build();

        FlashMap sessionAttributes = mockMvc.perform(post("/media")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("name=" + media.getName() + "&description=" + media.getDescription() + "&extension=" + media.getExtension().toString()))
                .andReturn().getFlashMap();

        mockMvc.perform(get("/media/show")
                .sessionAttrs(sessionAttributes))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("show-photo"))
                .andExpect(model().attribute("media", allOf(
                        hasProperty("name", is(media.getName())),
                        hasProperty("description", is(media.getDescription())),
                        hasProperty("extension", is(media.getExtension())),
                        hasProperty("state", is(MediaStatus.State.PROCESSING)),
                        hasProperty("data", is(nullValue())))));
    }

    @Test
    public void when_VideoIsAddedWithData_Expect_VideoCorrectlyPersistedAndExtracted() throws Exception {
        Media media = new Media.MediaBuilder("video1")
                .withDescription("noDesc")
                .withExtension(MediaExtension.Extension.AVI)
                .build();
        MockMultipartFile mockedFile = new MockMultipartFile("file", (media.getName() + "." + media.getExtension().getDisplayName()),
                MediaType.MULTIPART_FORM_DATA_VALUE, media.getName().getBytes());

        FlashMap sessionAttributes = mockMvc.perform(post("/media")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("name=" + media.getName() + "&description=" + media.getDescription() + "&extension=" + media.getExtension().toString()))
                .andReturn().getFlashMap();
        sessionAttributes = mockMvc.perform(multipart("/media/upload")
                .file(mockedFile)
                .sessionAttrs(sessionAttributes)
                .param("name", media.getName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/media/show"))
                .andReturn().getFlashMap();

        media = mediaService.retrieveMedia(media.getName());
        String encodedContent = mediaService.retrieveEncodedData(media);
        mockMvc.perform(get("/media/show?name=" + media.getName())
                .sessionAttrs(sessionAttributes))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("show-video"))
                .andExpect(model().attribute("media", allOf(
                        hasProperty("name", is(media.getName())),
                        hasProperty("description", is(media.getDescription())),
                        hasProperty("extension", is(media.getExtension())),
                        hasProperty("state", is(MediaStatus.State.PROCESSED)),
                        hasProperty("data", is(notNullValue())))))
                .andExpect(content().string(containsString(encodedContent)));
        assertThat(Base64.getDecoder().decode(encodedContent), is(mockedFile.getBytes()));
    }

    @Test
    public void when_VideoIsAddedWithoutData_Expect_VideoInformationAccessibleWithoutData() throws Exception {
        Media media = new Media.MediaBuilder("video2")
                .withDescription("noDesc")
                .withExtension(MediaExtension.Extension.AVI)
                .build();

        FlashMap sessionAttributes = mockMvc.perform(post("/media")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("name=" + media.getName() + "&description=" + media.getDescription() + "&extension=" + media.getExtension().toString()))
                .andReturn().getFlashMap();

        mockMvc.perform(get("/media/show")
                .sessionAttrs(sessionAttributes))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("show-video"))
                .andExpect(model().attribute("media", allOf(
                        hasProperty("name", is(media.getName())),
                        hasProperty("description", is(media.getDescription())),
                        hasProperty("extension", is(media.getExtension())),
                        hasProperty("state", is(MediaStatus.State.PROCESSING)),
                        hasProperty("data", is(nullValue())))));
    }

    @Test
    public void when_AudioIsAddedWithData_Expect_AudioCorrectlyPersistedAndExtracted() throws Exception {
        Media media = new Media.MediaBuilder("audio1")
                .withDescription("noDesc")
                .withExtension(MediaExtension.Extension.MP3)
                .build();
        MockMultipartFile mockedFile = new MockMultipartFile("file", (media.getName() + "." + media.getExtension().getDisplayName()),
                MediaType.MULTIPART_FORM_DATA_VALUE, media.getName().getBytes());

        FlashMap sessionAttributes = mockMvc.perform(post("/media")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("name=" + media.getName() + "&description=" + media.getDescription() + "&extension=" + media.getExtension().toString()))
                .andReturn().getFlashMap();
        sessionAttributes = mockMvc.perform(multipart("/media/upload")
                .file(mockedFile)
                .sessionAttrs(sessionAttributes)
                .param("name", media.getName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/media/show"))
                .andReturn().getFlashMap();

        media = mediaService.retrieveMedia(media.getName());
        String encodedContent = mediaService.retrieveEncodedData(media);
        mockMvc.perform(get("/media/show")
                .sessionAttrs(sessionAttributes))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("show-audio"))
                .andExpect(model().attribute("media", allOf(
                        hasProperty("name", is(media.getName())),
                        hasProperty("description", is(media.getDescription())),
                        hasProperty("extension", is(media.getExtension())),
                        hasProperty("state", is(MediaStatus.State.PROCESSED)),
                        hasProperty("data", is(notNullValue())))))
                .andExpect(content().string(containsString(encodedContent)));
        assertThat(Base64.getDecoder().decode(encodedContent), is(mockedFile.getBytes()));
    }

    @Test
    public void when_AudioIsAddedWithoutData_Expect_AudioInformationAccessibleWithoutData() throws Exception {
        Media media = new Media.MediaBuilder("audio2")
                .withDescription("noDesc")
                .withExtension(MediaExtension.Extension.MP3)
                .build();

        FlashMap sessionAttributes = mockMvc.perform(post("/media")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("name=" + media.getName() + "&description=" + media.getDescription() + "&extension=" + media.getExtension().toString()))
                .andReturn().getFlashMap();

        mockMvc.perform(get("/media/show")
                .sessionAttrs(sessionAttributes))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("show-audio"))
                .andExpect(model().attribute("media", allOf(
                        hasProperty("name", is(media.getName())),
                        hasProperty("description", is(media.getDescription())),
                        hasProperty("extension", is(media.getExtension())),
                        hasProperty("state", is(MediaStatus.State.PROCESSING)),
                        hasProperty("data", is(nullValue())))));
    }


    @AfterClass
    public static void tearDown() {
        mongoDBContainer.stop();
    }

}
