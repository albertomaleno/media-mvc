/*
 * Copyright (C) 2020 alberto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.myhouse.mvc.controller;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.Mockito.*;

import com.myhouse.mvc.model.Media;
import com.myhouse.mvc.model.MediaExtension;
import com.myhouse.mvc.service.MediaService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.FlashMap;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MediaControllerTest {

    @Mock
    private MediaService mockedMediaService;

    @InjectMocks
    private MediaController mediaController;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setUp(){
        mockMvc = MockMvcBuilders.standaloneSetup(mediaController).build();
    }


    @Test
    public void when_GetIsPerformedToAddMedia_Expect_MediaFormView() throws Exception {
        mockMvc.perform(get("/media"))
                .andExpect(status().isOk())
                .andExpect(view().name("add-media-information"));
    }


    @Test
    public void when_PostIsPerformedToAddMediaWithForm_Expect_RedirectToAddData() throws Exception {
        Media media = new Media.MediaBuilder("photo")
                .withDescription("noDesc")
                .withExtension(MediaExtension.Extension.JPG)
                .build();

        doNothing().when(mockedMediaService).saveMedia(media);

        mockMvc.perform(post("/media")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("name=" + media.getName() + "&description=" + media.getDescription() + "&extension=" + media.getExtension().toString()))
                .andExpect(redirectedUrl("/media/upload"));
    }


    @Test
    public void when_PostIsPerformedToAddMediaWithNullParameters_Expect_Error() throws Exception {
        mockMvc.perform(post("/media")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("name&description&extension"))
                .andExpect(view().name("add-media-information"))
                .andExpect(model().attributeHasFieldErrorCode("media", "name", "NotEmpty"))
                .andExpect(model().attributeHasFieldErrorCode("media", "extension", "NotNull"));
    }


    @Test
    public void when_GetIsPerformedToUploadMedia_Expect_RedirectToUploadMediaData() throws Exception {
        mockMvc.perform(get("/media/upload"))
                .andExpect(view().name("upload-media-data"));
    }

    @Test
    public void when_MultipartIsPerformedToUploadMediaWithValidMediaName_Expect_RedirectToShowMedia() throws Exception {
        Media media = new Media.MediaBuilder("photo")
                .withDescription("noDesc")
                .withExtension(MediaExtension.Extension.JPG)
                .build();
        MockMultipartFile mockedFile = new MockMultipartFile(media.getName(), (media.getName() + ".jpg"),
                MediaType.MULTIPART_FORM_DATA_VALUE, media.getName().getBytes());
        FlashMap sessionAttributes = mockMvc.perform(post("/media")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("name=" + media.getName() + "&description=" + media.getDescription() + "&extension=" + media.getExtension().toString()))
                .andExpect(redirectedUrl("/media/upload"))
        .andReturn().getFlashMap();

        when(mockedMediaService.retrieveMedia(media.getName())).thenReturn(media);

        mockMvc.perform(multipart("/media/upload")
                .file("file", mockedFile.getBytes())
                .sessionAttrs(sessionAttributes))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/media/show"));
    }

    @Test
    public void when_GetIsPerformedToShowMediaForAPhoto_Expect_RedirectToShowPhoto() throws Exception {
        Media media = new Media.MediaBuilder("photo")
                .withDescription("noDesc")
                .withExtension(MediaExtension.Extension.JPG)
                .build();
        FlashMap sessionAttributes = mockMvc.perform(post("/media")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("name=" + media.getName() + "&description=" + media.getDescription() + "&extension=" + media.getExtension().toString()))
                .andExpect(redirectedUrl("/media/upload"))
                .andReturn().getFlashMap();

        mockMvc.perform(get("/media/show")
                .sessionAttrs(sessionAttributes))
                .andExpect(view().name("show-photo"));
    }

    @Test
    public void when_GetIsPerformedToShowMediaForAVideo_Expect_RedirectToShowVideo() throws Exception {
        Media media = new Media.MediaBuilder("video")
                .withDescription("noDesc")
                .withExtension(MediaExtension.Extension.MKV)
                .build();
        FlashMap sessionAttributes = mockMvc.perform(post("/media")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("name=" + media.getName() + "&description=" + media.getDescription() + "&extension=" + media.getExtension().toString()))
                .andExpect(redirectedUrl("/media/upload"))
                .andReturn().getFlashMap();

        mockMvc.perform(get("/media/show")
                .sessionAttrs(sessionAttributes))
                .andExpect(view().name("show-video"));
    }

    @Test
    public void when_GetIsPerformedToShowMediaForAnAudio_Expect_RedirectToShowAudio() throws Exception {
        Media media = new Media.MediaBuilder("audio")
                .withDescription("noDesc")
                .withExtension(MediaExtension.Extension.MP3)
                .build();
        FlashMap sessionAttributes = mockMvc.perform(post("/media")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("name=" + media.getName() + "&description=" + media.getDescription() + "&extension=" + media.getExtension().toString()))
                .andExpect(redirectedUrl("/media/upload"))
                .andReturn().getFlashMap();

        mockMvc.perform(get("/media/show")
                .sessionAttrs(sessionAttributes))
                .andExpect(view().name("show-audio"));
    }


    @Test
    public void when_GetIsPerformedToShowMedia_Expect_ModelAttributesFilled() throws Exception {
        Media media = new Media.MediaBuilder("photo")
                .withDescription("noDesc")
                .withExtension(MediaExtension.Extension.JPG)
                .build();
        FlashMap sessionAttributes = mockMvc.perform(post("/media")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("name=" + media.getName() + "&description=" + media.getDescription() + "&extension=" + media.getExtension().toString()))
                .andExpect(redirectedUrl("/media/upload"))
                .andReturn().getFlashMap();

        when(mockedMediaService.retrieveMedia(media.getName())).thenReturn(media);

        mockMvc.perform(get("/media/show")
            .sessionAttrs(sessionAttributes))
                .andExpect(status().isOk())
                .andExpect(model().attribute("media", allOf(
                        hasProperty("name", is(media.getName())),
                        hasProperty("description", is(media.getDescription())),
                        hasProperty("extension", is(media.getExtension())),
                        hasProperty("extension", not(media.getExtension().getDisplayName())))));
    }

}
