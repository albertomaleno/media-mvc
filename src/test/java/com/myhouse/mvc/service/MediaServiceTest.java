/*
 * Copyright (C) 2020 alberto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.myhouse.mvc.service;

import static org.mockito.Mockito.*;

import com.myhouse.mvc.dal.interfaces.MediaDAO;
import com.myhouse.mvc.model.Media;
import com.myhouse.mvc.model.MediaExtension;
import com.myhouse.mvc.model.MediaStatus;
import com.myhouse.mvc.validation.abstracts.AbstractMediaValidator;
import org.bson.types.Binary;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.Base64;

@RunWith(SpringRunner.class)
public class MediaServiceTest {

    private MediaDAO mockedMediaDao;
    private AbstractMediaValidator mockedMediaValidator;
    private MediaService mediaService;
    private Media media;

    @Before
    public void setUp() {
        mockedMediaDao = mock(MediaDAO.class);
        mockedMediaValidator = mock(AbstractMediaValidator.class);
        mediaService = new MediaService(mockedMediaDao, mockedMediaValidator);
        media = new Media();
        media.setId("1");
        media.setName("photo");
        media.setDescription("This is a description");
        media.setExtension(MediaExtension.Extension.PNG);
        media.setState(MediaStatus.State.PROCESSING);
    }

    @Test
    public void when_MediaIsSaved_Expect_MediaPersisted() {
        doNothing().when(mockedMediaDao).persist(media);
        when(mockedMediaDao.findByName(media.getName())).thenReturn(media);

        mediaService.saveMedia(media);
        Assert.assertTrue(media.equals(mediaService.retrieveMedia(media.getName())));
    }

    @Test
    public void when_MediaIsSaved_Expect_MediaToBeRetrievable() {
        doNothing().when(mockedMediaDao).persist(media);
        when(mockedMediaDao.findByName(media.getName())).thenReturn(media);

        Assert.assertTrue(media.equals(mediaService.retrieveMedia(media.getName())));
    }

    @Test
    public void when_MediaInformationExist_Expect_MediaDataSavedAsProcessed() throws IOException, CloneNotSupportedException {
        CloneableMedia cloneableMedia = new CloneableMedia();
        cloneableMedia.setId("1");
        cloneableMedia.setName("media");
        cloneableMedia.setState(MediaStatus.State.PROCESSING);
        cloneableMedia.setExtension(MediaExtension.Extension.FLAAC);
        MockMultipartFile mockedMultipartFile = new MockMultipartFile("media", "media.flaac", "image/png", "media".getBytes());
        Media expectedMedia = (Media) cloneableMedia.clone();
        expectedMedia.setData(new Binary(mockedMultipartFile.getBytes()));
        expectedMedia.setState(MediaStatus.State.PROCESSED);

        when(mockedMediaDao.findByName(cloneableMedia.getName())).thenReturn(cloneableMedia);
        when(mockedMediaValidator.isSame(cloneableMedia, mockedMultipartFile)).thenReturn(true);
        doNothing().when(mockedMediaDao).persist(cloneableMedia);

        mediaService.saveMediaData(cloneableMedia.getName(), mockedMultipartFile);
        Assert.assertTrue(mediaService.retrieveMedia(cloneableMedia.getName()).equals(expectedMedia));
    }


    @Test(expected = ResponseStatusException.class)
    public void when_MediaInformationDoesNotExist_Expect_ServiceException() throws IOException {
        final String fakeMediaName = "fake";
        MockMultipartFile mockedMultipartFile = new MockMultipartFile("fake", "fake.png", "image/png", "media".getBytes());

        when(mockedMediaDao.findByName(fakeMediaName)).thenReturn(null);

        mediaService.saveMediaData(fakeMediaName, mockedMultipartFile);
    }


    @Test
    public void when_MediaExist_Expect_MediaToBeFound() {
        when(mockedMediaDao.findByName(media.getName())).thenReturn(media);

        Assert.assertTrue(mediaService.retrieveMedia(media.getName()).equals(media));
    }

    @Test(expected = ResponseStatusException.class)
    public void when_MediaDoesNotExist_Expect_ServiceException() {
        final String fakeMediaName = "fake";
        when(mockedMediaDao.findByName(fakeMediaName)).thenReturn(null);

        mediaService.retrieveMedia(fakeMediaName);
    }

    @Test
    public void when_MediaDataExist_Expect_MediaDataEncodedAsBase64() throws IOException {
        MockMultipartFile mockedMultipartFile = new MockMultipartFile("media", "media.png", "image/png", "media".getBytes());
        media.setData(new Binary(mockedMultipartFile.getBytes()));
        media.setState(MediaStatus.State.PROCESSED);

        when(mockedMediaDao.findByName(media.getName())).thenReturn(media);

        Assert.assertTrue(mediaService.retrieveEncodedData(media).equals(Base64.getEncoder().encodeToString(media.getData().getData())));
    }

    @Test
    public void when_MediaExist_Expect_MediaToBeRemoved() {
        when(mockedMediaDao.findByName(media.getName())).thenReturn(media);
        doNothing().when(mockedMediaDao).remove(media);

        mediaService.removeMedia(media.getName());
    }



    class CloneableMedia extends Media implements Cloneable {
        @Override
        protected Object clone() throws CloneNotSupportedException {
            return super.clone();
        }
    }

}
