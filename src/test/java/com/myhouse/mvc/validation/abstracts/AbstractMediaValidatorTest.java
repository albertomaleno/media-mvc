package com.myhouse.mvc.validation.abstracts;

import static org.junit.Assert.*;

import com.myhouse.mvc.model.Media;
import com.myhouse.mvc.model.MediaExtension;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.mock.web.MockMultipartFile;

public class AbstractMediaValidatorTest {

    private AbstractMediaValidator abstractMediaValidator;

    @Before
    public void setUp(){
        abstractMediaValidator = Mockito.spy(AbstractMediaValidator.class);
    }

    @Test
    public void when_mediaModelIsNull_Expect_HasSameNameIsFalse(){
        Media media = null;
        MockMultipartFile mockedMultipartFile = new MockMultipartFile("media", "media.png", "image/png", "media".getBytes());

        boolean hasSameName = abstractMediaValidator.hasSameName(media, mockedMultipartFile);

        assertFalse(hasSameName);
    }

    @Test
    public void when_mediaDataIsNull_Expect_HasSameNameIsFalse(){
        Media media = new Media();
        MockMultipartFile mockedMultipartFile = null;

        boolean hasSameName = abstractMediaValidator.hasSameName(media, mockedMultipartFile);

        assertFalse(hasSameName);
    }

    @Test
    public void when_mediaModelAndDataHaveSameName_Expect_HasSameNameIsTrue(){
        Media media = new Media();
        media.setName("media");
        MockMultipartFile mockedMultipartFile = new MockMultipartFile("media", "media.png", "image/png", "media".getBytes());


        boolean hasSameName = abstractMediaValidator.hasSameName(media, mockedMultipartFile);

        assertTrue(hasSameName);
    }

    @Test
    public void when_mediaModelIsNull_Expect_HasSameExtensionIsFalse(){
        Media media = null;
        MockMultipartFile mockedMultipartFile = new MockMultipartFile("media", "media.png", "image/png", "media".getBytes());

        boolean hasSameExtension = abstractMediaValidator.hasSameExtension(media, mockedMultipartFile);

        assertFalse(hasSameExtension);
    }

    @Test
    public void when_mediaDataIsNull_Expect_HasSameExtensionIsFalse(){
        Media media = new Media();
        MockMultipartFile mockedMultipartFile = null;

        boolean hasSameExtension = abstractMediaValidator.hasSameExtension(media, mockedMultipartFile);

        assertFalse(hasSameExtension);
    }

    @Test
    public void when_mediaModelAndDataHaveSameExtension_Expect_HasSameExtensionIsTrue(){
        Media media = new Media();
        media.setExtension(MediaExtension.Extension.PNG);
        MockMultipartFile mockedMultipartFile = new MockMultipartFile("media", "media.png", "image/png", "media".getBytes());

        boolean hasSameName = abstractMediaValidator.hasSameName(media, mockedMultipartFile);

        assertFalse(hasSameName);
    }

}
