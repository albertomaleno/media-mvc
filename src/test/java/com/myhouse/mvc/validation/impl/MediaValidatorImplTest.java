package com.myhouse.mvc.validation.impl;

import static org.junit.Assert.*;

import com.myhouse.mvc.model.Media;
import com.myhouse.mvc.model.MediaExtension;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;

public class MediaValidatorImplTest {

    private MediaValidatorImpl mediaValidator = new MediaValidatorImpl();

    @Test
    public void when_MediaModelIsNull_Expect_IsSameIsFalse(){
        Media media = null;
        MockMultipartFile mockedMultipartFile = new MockMultipartFile("media", "media.png", "image/png", "media".getBytes());

        boolean isSame = mediaValidator.isSame(media, mockedMultipartFile);

        assertFalse(isSame);
    }

    @Test
    public void when_MediaDataIsNull_Expect_IsSameIsFalse(){
        Media media = new Media();
        MockMultipartFile mockedMultipartFile = null;

        boolean isSame = mediaValidator.isSame(media, mockedMultipartFile);

        assertFalse(isSame);
    }

    @Test
    public void when_MediaModelAndDataAreTheSame_Expect_IsSameIsTrue(){
        Media media = new Media();
        media.setName("media");
        media.setExtension(MediaExtension.Extension.PNG);
        MockMultipartFile mockedMultipartFile = new MockMultipartFile("media", "media.png", "image/png", "media".getBytes());

        boolean isSame = mediaValidator.isSame(media, mockedMultipartFile);

        assertTrue(isSame);
    }
}
