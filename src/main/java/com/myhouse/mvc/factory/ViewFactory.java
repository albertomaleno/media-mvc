/*
 * Copyright (C) 2020 alberto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.myhouse.mvc.factory;

import com.myhouse.mvc.model.Media;
import com.myhouse.mvc.model.MediaExtension;

/**
 * Class that decides which view must be returned when showing media information
 * by matching against possible media extensions
 *
 * @author alberto
 */
public class ViewFactory {

    private static final String PHOTO_VIEW = "show-photo";
    private static final String VIDEO_VIEW = "show-video";
    private static final String AUDIO_VIEW = "show-audio";

    /**
     * Gets the corresponding view for media by extension
     *
     * @param media
     * @return mediaView
     */
    public static String getView(Media media) {
        MediaExtension.Extension mediaExtension = media.getExtension();

        if (isAnyOf(mediaExtension, MediaExtension.Extension.PNG, MediaExtension.Extension.JPG, MediaExtension.Extension.GIF, MediaExtension.Extension.RAW)) {
            return PHOTO_VIEW;
        } else if (isAnyOf(mediaExtension, MediaExtension.Extension.MKV, MediaExtension.Extension.AVI, MediaExtension.Extension.MP4)) {
            return VIDEO_VIEW;
        } else {
            return AUDIO_VIEW;
        }
    }


    private static boolean isAnyOf(MediaExtension.Extension inputExtension, MediaExtension.Extension... expectedExtensions) {
        boolean isAnyOf = false;
        for (MediaExtension.Extension extension : expectedExtensions) {
            if (inputExtension.equals(extension)) {
                isAnyOf = true;
                break;
            }
        }
        return isAnyOf;
    }
}
