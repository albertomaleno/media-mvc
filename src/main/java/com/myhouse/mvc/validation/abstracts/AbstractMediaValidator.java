/*
 * Copyright (C) 2020 alberto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.myhouse.mvc.validation.abstracts;

import com.myhouse.mvc.model.Media;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

/**
 * Class that validates media model against it's data.
 * Extension classes must decide how to validate if media
 * model and multipart file are the same by using some of already
 * created methods or by creating new ones
 *
 * @author alberto
 */
public abstract class AbstractMediaValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractMediaValidator.class);

    /**
     * Validates if multipart file information equals to media model
     *
     * @param media is the model
     * @param multipartFile is the media data
     * @return isSame
     */
    public abstract boolean isSame(Media media, MultipartFile multipartFile);

    /**
     * Validates media model has the same name as multipart file
     *
     * @param media is the media model
     * @param multipartFile is the media data
     * @return hasSameName
     */
    public boolean hasSameName(Media media, MultipartFile multipartFile){
        boolean isSame = false;

        try{
            String mediaName = media.getName();
            String[] multipartOriginalFileName = multipartFile.getOriginalFilename().split("\\.");
            String multipartFileName = multipartOriginalFileName[0];

            if (mediaName.equalsIgnoreCase(multipartFileName)){
                isSame = true;
            }

        }catch (NullPointerException e){
            LOGGER.error("MultipartFile has no name defined");
        }
        return isSame;
    }

    /**
     * Validates media model has the same extension as multipart file
     *
     * @param media is the model
     * @param multipartFile is the media data
     * @return hasSameExtension
     */
    public boolean hasSameExtension(Media media, MultipartFile multipartFile){
        boolean isSame = false;

        try{
            String mediaExtension= media.getExtension().name();
            String[] multipartOriginalFileName = multipartFile.getOriginalFilename().split("\\.");
            String multipartExtension = multipartOriginalFileName[1];

            if (mediaExtension.equalsIgnoreCase(multipartExtension)){
                isSame = true;
            }

        }catch (NullPointerException e){
            LOGGER.error("MultipartFile has no extension defined");
        }
        return isSame;
    }



}
