/*
 * Copyright (C) 2020 alberto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.myhouse.mvc.validation.impl;

import com.myhouse.mvc.model.Media;
import com.myhouse.mvc.validation.abstracts.AbstractMediaValidator;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * Class implementation to validate multipart file against its media model
 * by checking name and extension
 *
 * @author alberto
 */
@Component(value = "mediaValidator")
public class MediaValidatorImpl extends AbstractMediaValidator {

    @Override
    public boolean isSame(Media media, MultipartFile multipartFile) {
        return hasSameName(media, multipartFile) && hasSameExtension(media, multipartFile);
    }
}
