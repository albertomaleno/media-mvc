package com.myhouse.mvc.model;

/**
 * Class that encapsulates the possible extensions for media
 *
 * @author alberto
 */
public class MediaExtension {
    private final Extension mediaExtension;

    public MediaExtension(Extension mediaExtension) {
        this.mediaExtension = mediaExtension;
    }

    public enum Extension {
        JPG("jpg"),
        PNG("png"),
        RAW("raw"),
        GIF("gif"),
        MKV("mkv"),
        AVI("avi"),
        MP4("mp4"),
        MP3("mp3"),
        FLAAC("flaac");


        private final String displayName;

        Extension(String displayName) {
            this.displayName = displayName;
        }

        public String getDisplayName() {
            return displayName;
        }
    }
}
