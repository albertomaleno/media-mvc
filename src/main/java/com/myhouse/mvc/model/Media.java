/*
 * Copyright (C) 2020 alberto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.myhouse.mvc.model;


import javax.validation.constraints.*;

import com.myhouse.mvc.validation.interfaces.EnumValueOf;
import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import java.util.Objects;
import com.myhouse.mvc.model.MediaExtension.Extension;
import com.myhouse.mvc.model.MediaStatus.State;

/**
 * @author alberto
 */
public class Media {

    @Id
    private String id;
    @NotEmpty(message = "Media name can not be empty")
    @Size(max = 40, message = "Media name can not have more than 40 characters")
    private String name;
    @Size(max = 140, message = "Media description exceeds the maximum of 140 characters.")
    private String description;
    @NotNull
    @EnumValueOf(enumClass = MediaExtension.Extension.class)
    private Extension extension;
    @Field(name = "file")
    private Binary data;
    private State mediaState;

    public Media() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public State getState() {
        return mediaState;
    }

    public void setState(MediaStatus.State mediaState) {
        this.mediaState = mediaState;
    }

    public Extension getExtension() {
        return extension;
    }

    public void setExtension(Extension extension) {
        this.extension = extension;
    }

    public Binary getData() {
        return data;
    }

    public void setData(Binary data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Media media = (Media) o;
        return Objects.equals(id, media.id) &&
                Objects.equals(name, media.name) &&
                Objects.equals(description, media.description) &&
                Objects.equals(extension, media.extension) &&
                mediaState == media.mediaState;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, extension, mediaState);
    }

    @Override
    public String toString() {
        return "Media{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", extension=" + extension +
                ", data=" + data +
                ", mediaState=" + mediaState +
                '}';
    }

    public static class MediaBuilder{

        private final String name;
        private String description = "";
        private Extension extension = Extension.JPG;
        private State mediaState = State.PROCESSING;

        public MediaBuilder(String name){
            this.name = name;
        }

        public MediaBuilder withDescription(String description){
            this.description = description;
            return this;
        }

        public MediaBuilder withExtension(Extension extension){
            this.extension = extension;
            return this;
        }

        public MediaBuilder withMediaState(State mediaState){
            this.mediaState = mediaState;
            return this;
        }

        public Media build(){
            return new Media(this);
        }
    }


    private Media(MediaBuilder mediaBuilder){
        this.name = mediaBuilder.name;
        this.description = mediaBuilder.description;
        this.mediaState = mediaBuilder.mediaState;
        this.extension = mediaBuilder.extension;
    }
}
