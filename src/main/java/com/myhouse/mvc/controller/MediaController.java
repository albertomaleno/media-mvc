/*
 * Copyright (C) 2020 alberto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.myhouse.mvc.controller;

import com.myhouse.mvc.factory.ViewFactory;
import com.myhouse.mvc.model.Media;
import com.myhouse.mvc.service.MediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import java.io.IOException;


@Controller
@SessionAttributes("media")
public class MediaController {

    private final MediaService mediaService;

    @Autowired
    public MediaController(MediaService mediaService) {
        this.mediaService = mediaService;
    }


    @GetMapping("/media")
    public String createMediaForm(Model model) {
        model.addAttribute("media", new Media());
        return "add-media-information";
    }


    @PostMapping("/media")
    public String submitMediaForm(@Valid Media media, Errors errors, RedirectAttributes redirectAttributes) {
        if (errors.hasErrors()) {
            return "add-media-information";
        }

        mediaService.saveMedia(media);

        redirectAttributes.addFlashAttribute("media", media);
        return "redirect:/media/upload";
    }

    @GetMapping("/media/upload")
    public String createMediaDataForm() {
        return "upload-media-data";
    }


    @PostMapping("/media/upload")
    public String submitMediaDataForm(@ModelAttribute("media") Media media, @RequestParam MultipartFile file, RedirectAttributes redirectAttributes) throws IOException {
        mediaService.saveMediaData(media.getName(), file);
        media = mediaService.retrieveMedia(media.getName());
        redirectAttributes.addFlashAttribute("media", media);
        return "redirect:/media/show";
    }


    @GetMapping("/media/show")
    public String showMedia(@ModelAttribute("media") Media media, Model model) {
        String data = mediaService.retrieveEncodedData(media);
        model.addAttribute("data", data);
        model.addAttribute("media", media);

        String view = ViewFactory.getView(media);

        return view;
    }
}

