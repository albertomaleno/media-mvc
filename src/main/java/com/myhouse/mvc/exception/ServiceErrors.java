/*
 * Copyright (C) 2020 alberto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.myhouse.mvc.exception;

/**
 * Enumeration of possible service errors
 *
 * @author alberto
 */
public enum ServiceErrors {

    MEDIA_DOES_NOT_EXIST(0, "No media could be found"),
    MEDIA_DATA_DIFFERS_FROM_MODEL(1, "Media data differs from information already present on database");

    private final int id;
    private final String message;

    ServiceErrors(int id, String message) {
        this.id = id;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }
}

