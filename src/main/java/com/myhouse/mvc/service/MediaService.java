/*
 * Copyright (C) 2020 alberto
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.myhouse.mvc.service;

import static com.myhouse.mvc.exception.ServiceErrors.*;

import com.myhouse.mvc.dal.interfaces.MediaDAO;
import com.myhouse.mvc.model.Media;
import com.myhouse.mvc.model.MediaStatus;
import com.myhouse.mvc.validation.abstracts.AbstractMediaValidator;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import java.io.IOException;
import java.util.Base64;

/**
 * Service that is responsible for managing the business logic about how to retrieve and store
 * media
 *
 * @author alberto
 */
@Service
public class MediaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MediaService.class);
    private MediaDAO mediaDAO;
    private AbstractMediaValidator mediaValidator;

    @Autowired
    public MediaService(MediaDAO mediaDAO, AbstractMediaValidator mediaValidator) {
        this.mediaDAO = mediaDAO;
        this.mediaValidator = mediaValidator;
    }

    /**
     * Saves media in processing state
     *
     * @param media
     */
    public void saveMedia(Media media) {
        LOGGER.debug("Saving media {}", media);
        long init = System.currentTimeMillis();
        media.setState(MediaStatus.State.PROCESSING);
        mediaDAO.persist(media);
        long end = System.currentTimeMillis();
        LOGGER.debug("Saved media in {} ms", (end - init));
    }

    /**
     * Saves media data in processed state if media information is present, validating that the model
     * information matches the information in the file provided
     *
     * @param mediaName
     * @param multipartFile
     * @throws ResponseStatusException not found if media is does not exist or bad request if
     * model information is different from file provided
     */
    public void saveMediaData(String mediaName, MultipartFile multipartFile) throws IOException {
        Media media = retrieveMedia(mediaName);

        if (mediaValidator.isSame(media, multipartFile)) {
            LOGGER.debug("Saving media data with size {}", multipartFile.getSize());
            media.setData(new Binary(BsonBinarySubType.BINARY, multipartFile.getBytes()));
            media.setState(MediaStatus.State.PROCESSED);
            mediaDAO.persist(media);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, MEDIA_DATA_DIFFERS_FROM_MODEL.getMessage());
        }
    }


    /**
     * Removes media from database if its present
     *
     * @param mediaName
     * @throws ResponseStatusException not found if media does not exist
     */
    public void removeMedia(String mediaName) {
        LOGGER.debug("Removing media with name {}", mediaName);
        Media media = retrieveMedia(mediaName);
        mediaDAO.remove(media);
    }

    /**
     * Retrieves media from database if its present
     *
     * @param mediaName
     * @return media
     * @throws ResponseStatusException not found if media does not exist
     */
    public Media retrieveMedia(String mediaName) {
        LOGGER.debug("Retrieving media with name {}", mediaName);
        long init = System.currentTimeMillis();
        Media media = mediaDAO.findByName(mediaName);

        if (media == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, MEDIA_DOES_NOT_EXIST.getMessage());
        }
        long end = System.currentTimeMillis();
        LOGGER.debug("Retrieved media in {} ms", (end - init));
        return media;
    }

    /**
     * Retrieves Base64 encoded media file if its present
     *
     * @param media
     * @return mediaData
     */
    public String retrieveEncodedData(Media media) {
        LOGGER.debug("Encoding media data with name {}", media.getName());
        return media.getData() != null ? Base64.getEncoder().encodeToString(media.getData().getData()) : "";
    }

}
