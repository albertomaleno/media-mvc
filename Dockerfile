FROM openjdk:8-jdk-alpine

VOLUME /tmp

ADD build/libs/media-mvc-1.0-SNAPSHOT.jar media-mvc.jar

EXPOSE 8080

ENTRYPOINT [ "sh", "-c", "java -Dspring.data.mongodb.host=${HOST} -Dspring.data.mongodb.port=${PORT} -jar /media-mvc.jar" ]
