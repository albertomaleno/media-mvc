# **media-mvc**

[![Build Status](https://travis-ci.com/albertomaleno/media-mvc.svg?branch=master)](https://travis-ci.com/albertomaleno/media-mvc) [![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) [![Coverage Status](https://coveralls.io/repos/github/albertomaleno/media-mvc/badge.svg?branch=master)](https://coveralls.io/github/albertomaleno/media-mvc?branch=master)



MVC to store and retrieve any kind of media made with Spring.

### Contents

- NoSQL database MongoDB
- Spring Boot
- Spring MVC
- Docker
- TDD



### Objectives

* Build a generic service to store media

- See how Spring can provide tools to improve development processes
- Apply MVC pattern
- Understand AOP
- Use Gradle as build tool
- Understand Unit, API and Integration testing
- Use a CI platform
- Use TestContainers to improve integration testing 
